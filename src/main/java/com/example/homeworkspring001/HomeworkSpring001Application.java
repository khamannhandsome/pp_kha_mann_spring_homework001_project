package com.example.homeworkspring001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeworkSpring001Application {

    public static void main(String[] args) {
        SpringApplication.run(HomeworkSpring001Application.class, args);
    }

}
