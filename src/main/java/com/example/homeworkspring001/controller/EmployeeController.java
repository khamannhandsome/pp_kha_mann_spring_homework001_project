package com.example.homeworkspring001.controller;

import com.example.homeworkspring001.model.Employee;
import com.example.homeworkspring001.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
public class EmployeeController {

    private final EmployeeRepository employeeRepository;

    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    Employee employee = new Employee(null,"mann","kha","mann@gmail.com",new Date(),"lol");

    @PostMapping("/insert employee")
    public Employee InsertEmployee(){


       return employeeRepository.saveEmployee(employee);
    }

    @GetMapping("/detach merge and insert new employee")
    public Employee detachAndMerge(){
//        Employee employee = new Employee(null,"detach","merge","success@gmail.com",null,"temp");
        return employeeRepository.detachAndMerge(employee);
    }

@GetMapping("find employee by Id/{employeeId}")
    public Employee getEmployeeById(@PathVariable Long employeeId){
        return employeeRepository.findById(employeeId);
    }

    @DeleteMapping("remove object and value in database/{id}")
    public String removeObjectAndValue(@PathVariable Long id){
        employeeRepository.removeObjectAndValue(id);
        return "success!";
    }



}
