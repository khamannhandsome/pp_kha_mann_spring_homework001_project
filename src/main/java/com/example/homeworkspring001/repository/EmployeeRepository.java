package com.example.homeworkspring001.repository;


import com.example.homeworkspring001.model.Employee;
import jakarta.persistence.EntityManager;
import jakarta.persistence.FlushModeType;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeRepository  {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Employee saveEmployee(Employee employee) {

         entityManager.persist(employee);
         return employee;
    }

    @Transactional
    public Employee detachAndMerge(Employee employee) {



        entityManager.detach(employee);

        employee.setFirstName("Nice");
        employee.setLastName("very nice");
//        Employee mergedEmployee = entityManager.merge(employee);
        entityManager.merge(employee);
//        entityManager.flush();

        return employee;
    }

//    @Transactional
//    public Optional<Employee> findById(Long employeeId) {
//        return entityManager.createQuery(
//                        "SELECT e FROM Employee e WHERE e.id = :employeeId", Employee.class)
//                .setParameter("employeeId",employeeId)
//                .getResultList()
//                .stream()
//                .findFirst();
//    }
    @Transactional
    public Employee findById(Long employeeId) {
        return entityManager.find(Employee.class,employeeId);
    }

    @Transactional
    public String removeObjectAndValue(Long id){
       Employee em= findById(id);
        entityManager.remove(em);
        return "removed success";
    }

}


