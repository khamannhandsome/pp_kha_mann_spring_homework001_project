package com.example.homeworkspring001.service;

import com.example.homeworkspring001.model.Employee;
import com.example.homeworkspring001.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EmployeeService {


        private final EmployeeRepository employeeRepository;
        public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    public void insertStudent(){
        Employee employee = new Employee();

        employeeRepository.saveEmployee(employee);

    }

//    public Optional<Employee> getEmpById(Long id){
//            return employeeRepository.findById(id);
//    }
}
